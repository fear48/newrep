'Use Strict';
angular.module('App').controller('loginController', function ($scope, $timeout, $ionicPopover, $window, $ionicHistory, $state, $localStorage, $location, $http, $ionicPopup, $ionicLoading, $ionicPlatform,  $firebaseObject, Auth, FURL, Utils) {
  var userkey = "";


var productIds = ['maigid', 'gidsale', 'com.ionicframework.gid', 'gid']; // <- Add your product Ids here
var spinner = '<ion-spinner icon="dots" class="spinner-stable"></ion-spinner><br/>';





  $scope.loadProducts = function () {

    google.payments.inapp.getSkuDetails(function(res){
      alert(res + 'res!');
    }, function(err){
      alert(err + 'error(');
    }, 'com.ionicframework.gid')




    $ionicLoading.show({ template: spinner + 'Loading Products...' });
    inAppPurchase
      .getProducts(productIds)
      .then(function (products) {
        $ionicLoading.hide();
        $scope.products = products;
        alert(products);
      })
      .catch(function (err) {
        $ionicLoading.hide();
        alert(err);
      });
  };

  $scope.buy = function (productId) {

    $ionicLoading.show({ template: spinner + 'Purchasing...' });
    inAppPurchase
      .buy(productId)
      .then(function (data) {
        console.log(JSON.stringify(data));
        console.log('consuming transactionId: ' + data.transactionId);
        return inAppPurchase.consume(data.type, data.receipt, data.signature);
      })
      .then(function () {
        var alertPopup = $ionicPopup.alert({
          title: 'Purchase was successful!',
          template: 'Check your console log for the transaction data'
        });
        console.log('consume done!');
        $ionicLoading.hide();
      })
      .catch(function (err) {
        $ionicLoading.hide();
        console.log(err);
        $ionicPopup.alert({
          title: 'Something went wrong',
          template: 'Check your console log for the error details'
        });
      });

  };

  $scope.restore = function () {
    $ionicLoading.show({ template: spinner + 'Restoring Purchases...' });
    inAppPurchase
      .restorePurchases()
      .then(function (purchases) {
        $ionicLoading.hide();
        console.log(JSON.stringify(purchases));
        $ionicPopup.alert({
          title: 'Restore was successful!',
          template: 'Check your console log for the restored purchases data'
        });
      })
      .catch(function (err) {
        $ionicLoading.hide();
        console.log(err);
        $ionicPopup.alert({
          title: 'Something went wrong',
          template: 'Check your console log for the error details'
        });
      });
  };
















var template = "<ion-popover-view class='popover-my' style='opacity: 1; width: 80%; padding: 15px; height: 150px !important;'>Здравствуйте уважаемый пользователь. Для удобства пользования приложением при регистрации рекомендуется вводить реальный номер телефона. Внимание – пароль не восстанавливается, его необходимо запомнить.<ion-content style='padding:15px'> </ion-content></ion-popover-view>";

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
  $ionicPopover.fromTemplateUrl(template, {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.isPop = false;

  $scope.openPopover = function($event) {
    $scope.popover.show($event);

    $scope.isPop = true;

  };
  $scope.closePopover = function() {
    $scope.popover.hide();

  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
    $scope.isPop = false;
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });






  
  $scope.signIn = function (user) {



    Firebase.goOnline();
    user.email = user.email + '@mail.com';

    
    console.log("Enviado");
    if(angular.isDefined(user)){
    Utils.show();
    Auth.login(user)
      .then(function(authData) {
      //console.log("id del usuario:" + JSON.stringify(authData));
      var ref = new Firebase('https://blinding-heat-2337.firebaseio.com/');

      console.log(authData);

      ref.child('profile').orderByChild("id").equalTo(authData.uid).on("child_added", function(snapshot) {
        console.log(snapshot.key());
        userkey = snapshot.key();

        var obj = $firebaseObject(ref.child('profile').child(userkey));
        
        obj.$loaded()
          .then(function(data) {
            //console.log(data === obj); // true
            //console.log(obj.email);
            $localStorage.email = obj.email;
            $localStorage.userkey = userkey;
            $localStorage.uData = data;
            console.log($localStorage.uData);
            console.log($localStorage.uData.mode);

              Utils.hide();

              if($localStorage.uData.mode == 'Водитель автобуса'){
                  console.log('1');
                  $localStorage.uData = data;
                  $localStorage.isTaxi = false;
                  $localStorage.isMan = false;
                  $localStorage.isBus = true;
                  $state.go('tab.bus', {}, {reload: true});
                  $location.path('/tab/bus');



               } else if($localStorage.uData.mode == 'Водитель такси'){
                  console.log('2');
                  $localStorage.uData = data;
                  $localStorage.isBus = false;
                  $localStorage.isMan = false;
                  $localStorage.isTaxi = true;
                  $state.go('tab.taxi', {}, {reload: true});
                  $location.path('/tab/taxi');

               } else if($localStorage.uData.mode == 'Пешеход'){
                  console.log('3');
                  $localStorage.uData = data;

                  $localStorage.isMan = true;
                  $localStorage.isTaxi = false;
                  $localStorage.isBus = false;
                  $location.path('/tab/account');

                  $state.go('tab.account');
               }
              
              console.log("Starter page","Home");
              console.log($localStorage.uData);

          })
          .catch(function(error) {
            console.error("Error:", error);
          });
      });

      }, function(err) {
        Utils.hide();
         Utils.errMessage(err);
      });
    }
  };

});
