angular.module('App.controllers', ['leaflet-directive', 'jett.ionic.filter.bar'])

.controller('profileController', function($scope, $ionicPopover, $rootScope, $timeout, $state, $window, $ionicHistory, $location, $http, $localStorage, $ionicPopup, $firebaseObject, Auth, FURL, Utils){
  
  var ref = new Firebase('https://blinding-heat-2337.firebaseio.com/profile');
  var reference = new Firebase('https://blinding-heat-2337.firebaseio.com');
  var reference2 = new Firebase('https://blinding-heat-2337.firebaseio.com/taxi');
  var reference3 = new Firebase('https://blinding-heat-2337.firebaseio.com/markers');
  var reference4 = new Firebase('https://blinding-heat-2337.firebaseio.com/profile');

var template = "<ion-popover-view class='popover-my'><ion-content style='padding:15px'> </ion-content></ion-popover-view>";

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
  $ionicPopover.fromTemplateUrl(template, {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.isPop = false;

  $scope.openPopover = function($event) {
    $scope.popover.show($event);

    $scope.isPop = true;

  };
  $scope.closePopover = function() {
    $scope.popover.hide();

  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
    $scope.isPop = false;
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });






if($rootScope.shareState){
    $scope.shareState = false;
    $scope.shareText = 'Стоп';

}else{
    $rootScope.shareState = true;
    $rootScope.shareText = 'Старт';
 }

 if($rootScope.wanted){
    $rootScope.wanted = true;
 }else{
    $rootScope.wanted = false;
 }
    $scope.user = $localStorage.uData;

console.log($localStorage);




if($localStorage.uData.mode == 'Водитель автобуса'){
    $scope.isBus = true;
}else if($localStorage.uData.mode == 'Пешеход'){
    $scope.isMan = true;
}else if($localStorage.uData.mode == 'Водитель такси'){
    $scope.isTaxi = true;
 }


  $scope.phone = $localStorage.uData.email.match(/^[0-9]+/);
  //console.log(window.localStorage['ngStorage-uData'].match(/Водитель такси/gmi)[0]);


$rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
      $rootScope.backState = from.name;
      console.log(from.name);
      navigator.geolocation.clearWatch($rootScope.positionTimer);
      navigator.geolocation.clearWatch($rootScope.nav);
      console.log('watch cleared');
});


  $scope.backTo = function(){
    // if($localStorage.uData.mode == 'Водитель автобуса'){
    //   $location.path("/tab/bus");
    // }else if($localStorage.uData.mode == 'Водитель такси'){
    //   $location.path("/tab/taxi");
    // }else if($localStorage.uData.mode =='Пешеход'){

      console.log($rootScope.backState);
       if($rootScope.backState){
          $state.go($rootScope.backState);
       } else{
                if($localStorage.uData.mode == 'Водитель автобуса'){
                $location.path("/tab/bus");
              }else if($localStorage.uData.mode == 'Водитель такси'){
                $location.path("/tab/taxi");
              }else if($localStorage.uData.mode =='Пешеход'){
                $state.go("tab.account");
              }
       }
  };

  $scope.deleteAcc = function(){

  var ref = new Firebase('https://blinding-heat-2337.firebaseio.com/profile');
    var num = $localStorage.uData.email;

    $scope.newpass = {};

    var confirmPopup = $ionicPopup.show({
    template: "<div class='list list-inset'><label class='item item-input'><input type='text' ng-model='newpass.pass' placeholder='Введите пароль...'></label></div>",
    title: 'Внимание!',
    scope: $scope,
    buttons: [
      { text: 'Отмена' },
      {
        text: '<b>Удалить</b>',
        type: 'button-positive',
        onTap: function(e) {
          return true
        }
      }
    ]
  });

     confirmPopup.then(function(res) {
       if(res) {
          

          console.log($scope.newpass);
         
            console.log('removed');
              ref.removeUser({
                email: num,
                password: $scope.newpass.pass
              }, function(error) {
                if (error) {
                  switch (error.code) {
                    case "INVALID_USER":
                      console.log("Данный аккаунт не существует");
                      break;
                      case "INVALID_PASSWORD":
                      console.log("Вы ввели неверный пароль...");
                      Utils.alertshow("Ошибка!","Вы ввели неверный пароль...");
                      break;
                      default:
                      console.log("Ошибка удаления пользователя:", error);
                  }
                } else {
                  $scope.logOut();
                  console.log("Регистрация была успешно удалена!");
                }
              });
    

       } else {
         console.log('You are not sure');
       }
     });

  };


  $scope.logOut = function () {
        
    Firebase.goOffline();
    reference.unauth();
    reference4.unauth();
    navigator.geolocation.clearWatch($rootScope.positionTimer);

    if($localStorage.uData.mode == 'Пешеход'){
        console.log($localStorage.uData.id);
        var ref = new Firebase('https://blinding-heat-2337.firebaseio.com/taxi/clients/' + $localStorage.uData.id);
        console.log(ref);

        ref.remove();
    }


        Auth.logout();
        $localStorage.isTaxi = null;
        $localStorage.isBus = null;
        $localStorage.isMan = null;
        $localStorage = null;
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        console.log($localStorage);
        $location.path("/login");
        $state.go("login");
      }


})

.controller('BusCtrl', function($scope, leafletData, $ionicSideMenuDelegate, $rootScope, $timeout, $state, $cordovaGeolocation, $ionicPopover, $ionicModal, $window, $localStorage, $ionicPlatform, Utils) {



$scope.toggleLeft = function() {
      $ionicSideMenuDelegate.toggleLeft();
    };

  console.log($localStorage.uData);

    var currentPositionMarker;
    var fRef = new Firebase("https://blinding-heat-2337.firebaseio.com/markers");
    var geoFire = new GeoFire(fRef);
    var gf = geoFire.ref();
    var locIdIndex = 0;
    var locIds = new Array();
    var itemsProcessed = 0;



    var posOptions = {timeout: 1000,  frequency: 500};
    var tilesDict = {
        openstreetmap: {
            url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            options: {
                attribution: ''
            }
        },
        opencyclemap: {
            url: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",
            options: {
                attribution: 'All maps &copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, map data &copy; <a href="http://www.openstreetmap.org">OpenStreetMap</a> (<a href="http://www.openstreetmap.org/copyright">ODbL</a>'
            }
        },
        satellite: {
          url: "http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}",
          options: {
            attribution: '',
            subdomains: ['mt0','mt1','mt2','mt3'],
            maxZoom: 20
          }
        }
    };

    $scope.isBus = $localStorage.isBus;

    $scope.selectables = [
        1, 2, 3
      ];

$scope.busnum = $localStorage.uData.busnumber;

console.log($localStorage.uData.busnum);

  
if($localStorage.uData.mode == 'Пешеход'){
  leafletData.getMap('map').then(function(map) {
    var lc = L.control.locate({ follow: true,
      drawCircle: true,
      drawMarker: true,
      circleStyle: {
        fillOpacity: 0,
        weight: 2
      },
      locateOptions: {
        watch: true,
        maxZoom: 15
      }
      
    }).addTo(map);
    lc.start();
    console.log('set get');
  });
}





$scope.satMap = function(){
    $scope.map.tiles = tilesDict.satellite;
  };

  $scope.baseMap = function(){
     $scope.map.tiles = tilesDict.openstreetmap;
  };

    var template = "<ion-popover-view class='popover-my'><ion-content style='padding:15px'> </ion-content></ion-popover-view>";

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });

  // .fromTemplateUrl() method
  $ionicPopover.fromTemplateUrl(template, {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.isPop = false;

  $scope.openPopover = function($event) {
    $scope.popover.show($event);

    $scope.isPop = true;

  };
  $scope.closePopover = function() {
    $scope.popover.hide();

  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();


  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
    $scope.isPop = false;
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });


    console.log($scope.isBus);

        $scope.map = {
               san_fran: {
                      lat: 53.2219245,
                      lng: 63.609848799999995,
                      zoom: 12
                  },
                  markers: {
                  },
                  controls: {
                    
                  },
                  tiles: tilesDict.openstreetmap,
                  layers: {
                    overlays: {
                        22: {
                          type: 'group',
                          name: '22',
                          visible: true
                        },

                        3: {
                          type: 'group',
                          name: '3',
                          visible: true
                        },
                        2: {
                          type: 'group',
                          name: '2',
                          visible: true
                        },
                        11: {
                          type: 'group',
                          name: '11',
                          visible: true
                        }
                    }
                  },
                  defaults: {
                    scrollWhenZoom: false
                  }
                  

            }



$scope.toggleLayer = function(type){
  if(type){
      $scope.map.layers.overlays[type].visible = !$scope.map.layers.overlays[type].visible;
  }
}

$scope.vis = false;

$scope.routes = function(){
  $scope.vis = !$scope.vis;
};
           


    $scope.shareState = true;
    $scope.shareText = 'Старт';



  console.log($scope.map.markers);

  $scope.$on('$stateChangeStart', 
function(event, toState, toParams, fromState, fromParams){ 
  $scope.shareText = 'Старт';
  navigator.geolocation.clearWatch($rootScope.positionTimer);
  navigator.geolocation.clearWatch($rootScope.nav);
  geoFire.remove($localStorage.uData.id);
  if(typeof mid !== 'undefined'){
    $scope.map.markers[mid] = {};
  }
  $scope.map.markers['my'] = {};
  $scope.map.markers['undefined'] = {};
});


    $scope.toggleShare = function(){
      $scope.shareState = !$scope.shareState;
      console.log($scope.shareState);
      if($scope.shareState == true){
        $scope.shareText = 'Старт';
        var myid = $localStorage.uData.id;
        var mid = myid.replace(/-/gi, "");
        console.log(mid);
        navigator.geolocation.clearWatch($rootScope.positionTimer);
        navigator.geolocation.clearWatch($rootScope.nav);
        geoFire.remove($localStorage.uData.id);
        Firebase.goOffline();
        console.log($scope.map.markers);
        $scope.map.markers[mid] = {};
        $scope.map.markers['my'] = {};
        $scope.map.markers['undefined'] = {};
      }else{

    $scope.shareText = 'Стоп';

        if (navigator.geolocation) {
          Firebase.goOnline();
          $rootScope.nav = navigator.geolocation.getCurrentPosition(displayAndWatch, locError);
        } else {
            Utils.alertshow("Ошибка", "На вашем устройстве не поддерживается GPS");
            alert("На вашем устройстве не поддерживается GPS");
        }
      }
    };

    if($localStorage.uData.mode == 'Пешеход'){
      if(navigator.geolocation){

        
         // $timeout(function() {
         //    $scope.nav2 = navigator.geolocation.getCurrentPosition(displayAndWatch, locError);
         //  });
        

        
      }
    }
      
      

    // fRef.once('value', function(dataSnap){
    //   dataSnap.forEach(function(childSnap){
    //     locIds[locIdIndex++] = childSnap.key();
    //     itemsProcessed++;
    //     var myid = $localStorage.uData.uid;
    //     var mid = myid.replace(/-/gi, "");

    //     if(itemsProcessed == locIds.length){
    //       for(var i = 0; i < locIds.length; i++){
    //           geoFire.get(locIds[i]).then(function(location) {
    //           if (location === null) {
    //             console.log("Provided key is not in GeoFire");
    //           }
    //           else {
    //             console.log(mid);
    //             $scope.map.markers[mid] = {
    //               lat: location[0], 
    //               lng: location[1],
    //               message: 'Автобус',
    //               focus: 'false',
    //               icon: {
    //                 iconUrl: 'img/bus.png',
    //                 iconSize: [42, 42], 
    //               }
    //             };
    //             console.log($scope.map.markers);
    //             // console.log(childSnap.key()); id of marker
                
    //           }
    //         }, function(error) {
    //           console.log("Error: " + error);
    //         });
    //       }
    //     }
    //   });
    // });



    if($localStorage.uData.id != null){
      var dref = 'https://blinding-heat-2337.firebaseio.com/markers/' + $localStorage.uData.id;
      var disconnectRef = new Firebase(dref);
    }
          disconnectRef.onDisconnect().remove(function(){
          geoFire.remove($localStorage.uData.id);
          var myid = $localStorage.uData.id;
          var mid = myid.replace(/-/gi, "");

          if(typeof $scope.map.markers !== 'undefined'){
            if(typeof $scope.map.markers[mid] !== 'undefined'){
              console.log($scope.map.markers[mid]);
              $scope.map.markers[mid] = {};
            }
            
          }

        });
    
   
    // fRef.on('value', function(data){
    //   console.log(data.val());

    //   if(data.val()){
    //     if(data.val().busnumber){
    //       $scope.buses.push(data.val().busnumber);
    //       console.log($scope.buses);
    //     }
        
    //   }

    // });

$scope.buses ={};





  fRef.on('child_changed', function(childSnapshot, prevChildKey) {

    $timeout(function again() { 

      console.log(childSnapshot.val());
      console.log('child changed');
      console.log($localStorage.uData);
      var lat = childSnapshot.val().l[0];
      var lng = childSnapshot.val().l[1];

      var phonenum = $localStorage.uData.email.match(/\d+/g);


      console.log($scope.map.markers);

      console.log($localStorage.uData);
      //var html = "<span>"+ $localStorage.uData.name +"</span><br><span>"+ phonenum +"</span><br><button type='button' ng-click='doSomeAction()'>Позвонить</button>";

      if(childSnapshot.val().busnumber){
        var num = childSnapshot.val().busnumber.toString();
        console.log(typeof num);
        $scope.map.markers[childSnapshot.val().id] = {
            lat: lat,
            lng: lng,
            focus: false,
            layer: num,
            label: {
              message: num,
              options: {
                  noHide: true
              }
            },
            icon: {
              iconUrl: 'img/bus.png',
              iconSize: [32, 32]
            }
          }
        
      }

          
      
      

      console.log(lat);


       $timeout(1000); 
    }, 1000);

 $scope.isBuses = false;

  fRef.once('value', function(data){
  console.log(data.val());
  data.forEach(function(bus){
    

      $scope.buses[bus.key()] = bus.val();
      console.log($scope.buses);
      $scope.isBuses = true;
    
  });
});


  });


    


    fRef.on('child_removed', function(childSnap, prevChild){
       $scope.$apply(function () {

      var myid = $localStorage.uData.id;
      var mid = myid.replace(/-/gi, "");
      $scope.map.markers[childSnap.val().id] = {};

      $scope.buses[childSnap.key()] = {};
      //console.log($scope.map.markers[childSnap.val().id]);
      console.log('bus removed');

        });
    });



            function locError(error) {
                // the current position could not be located
                //alert("The current position could not be found!");
            }



          


            function setCurrentPosition(pos) {

              $scope.my_lat = pos.coords.latitude;
              $scope.my_lng = pos.coords.longitude;


              if($localStorage.uData.mode == 'Пешеход'){
                $scope.map.markers = {
                  "my": {
                    lat: $scope.my_lat,
                    lng: $scope.my_lng,
                    message: 'Это Я!',
                    focus: 'false',
                  }
                };
              } else{
                console.log($localStorage.uData.id);
                geoFire.set($localStorage.uData.id, [pos.coords.latitude, pos.coords.longitude]).then(function(){
                var myid = $localStorage.uData.id;
                var mid = myid.replace(/-/gi, "");
                  fRef.child($localStorage.uData.id).update({
                    'id': mid,
                    'busnumber': $localStorage.uData.busnumber
                  }); 
                });
              }
              

              
            }

           
            function displayAndWatch(position) {
              console.log(position);
                setCurrentPosition(position);
                watchCurrentPosition();
            }

            function watchCurrentPosition() {
   



                $rootScope.positionTimer = navigator.geolocation.watchPosition(
                  function (position) {
                      setMarkerPosition (position);
                      console.log('pos changed');
                      console.log(position.coords.latitude + position.coords.longitude);
                  }, locError, posOptions);
             


                 
              
            }

            function setMarkerPosition(position) {

              $scope.my_lat = position.coords.latitude;
              $scope.my_lng = position.coords.longitude;

              console.log('setMarker: ' + position.coords.latitude + position.coords.longitude);

             if($localStorage.uData.mode == 'Пешеход'){
                 $scope.map.markers["my"] = {
                    lat: $scope.my_lat,
                    lng: $scope.my_lng,
                    message: 'Это Я!',
                    focus: false
                };
              } else{

                geoFire.set($localStorage.uData.id, [position.coords.latitude, position.coords.longitude]);
                
                var myid = $localStorage.uData.id;
                var mid = myid.replace(/-/gi, "");

                fRef.child($localStorage.uData.id).update({
                  'id': mid,
                  'busnumber': $localStorage.uData.busnumber
                });
              }
             

 
               

                
            }

        //     function initLocationProcedure() {
        //         initializeMap();

        //         if (navigator.geolocation) {
        //           navigator.geolocation.getCurrentPosition(displayAndWatch, locError);

        //         } else {
        //             alert("Your browser does not support the Geolocation API");
        //         }
        //     }
        // initLocationProcedure();
    

})

.controller('TaxiCtrl', function($scope, $rootScope, $timeout, leafletData, $localStorage, $ionicHistory, $ionicPopover, $state, $cordovaGeolocation, Utils) {
    
    var myid = $localStorage.uData.id;
    var mid = myid.replace(/-/gi, "");
    var currentPositionMarker;
    var fRef = new Firebase("https://blinding-heat-2337.firebaseio.com/taxi");
    var geoFire = new GeoFire(fRef);
    var gf = geoFire.ref();
    var locIdIndex = 0;
    var locIds = new Array();
    var itemsProcessed = 0;
    var posOptions = {timeout: 1000, enableHighAccuracy: true, frequency: 500};
    var tilesDict = {
        openstreetmap: {
            url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            options: {
                attribution: ''
            }
        },
        opencyclemap: {
            url: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",
            options: {
                attribution: 'All maps &copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, map data &copy; <a href="http://www.openstreetmap.org">OpenStreetMap</a> (<a href="http://www.openstreetmap.org/copyright">ODbL</a>'
            }
        },
        satellite: {
          url: "http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}",
          options: {
            attribution: '',
            subdomains: ['mt0','mt1','mt2','mt3'],
            maxZoom: 20
          }
        }
    };
    var clients = new Firebase("https://blinding-heat-2337.firebaseio.com/taxi/clients");
    var cli = new GeoFire(clients);
    var phone = $localStorage.uData.email.match(/\d+/g);

    $scope.isTaxi = $localStorage.isTaxi;

    $ionicHistory.nextViewOptions({
        historyRoot: true
    });



    if($localStorage.uData.mode == 'Пешеход'){
      leafletData.getMap('map').then(function(map) {
        var lc = L.control.locate({ follow: true,
          drawCircle: true,
          drawMarker: true,
          color: '#000',
          circlePadding: [0,0],
          circleStyle: {
            fillOpacity: 0,
            weight: 2
          },
          locateOptions: {
            watch: true,
            maxZoom: 15
          }
          
        }).addTo(map);
        lc.start();
        console.log('set get');


      });
    }
    


   


    console.log($localStorage.uData);

        $scope.map = {
           san_fran: {
                  lat: 53.2219245,
                  lng: 63.609848799999995,
                  zoom: 12
              },
              markers: {},
              controls: {
                
                routingMachine: {
                  waypoints: [],
                  createMarkers: function() { return null; },
                  addWaypoints: false
                },
                createMarkers: function() { return null },
              },
              tiles: tilesDict.openstreetmap,
              defaults: {
                scrollWhenZoom: false
              }

        }

        if(typeof $scope.map.markers !== 'undefined'){

          clients.child($localStorage.uData.id).once("value", function(dataSnap){
            console.log(dataSnap.val());
             $scope.map.markers["my"] = {
              lat: dataSnap.val().l[0],
              lng: dataSnap.val().l[1],
              message: 'Это Я!',
              focus: false
            };
          });
        }
         

    if($localStorage.uData.id != null){
      var dref = 'https://blinding-heat-2337.firebaseio.com/taxi/' + $localStorage.uData.id;
      var dref2 = 'https://blinding-heat-2337.firebaseio.com/clients/' + $localStorage.uData.id;
      var disconnectRef = new Firebase(dref);
      var disconnectRef2 = new Firebase(dref2);
    }

  disconnectRef2.onDisconnect().remove(function(){
  //cli.remove($localStorage.uData.id);
  var myid = $localStorage.uData.id;
  var mid = myid.replace(/-/gi, "");

  if(typeof $scope.map.markers !== 'undefined'){
    if(typeof $scope.map.markers[mid] !== 'undefined'){
      console.log($scope.map.markers[mid]);
      $scope.map.markers[mid] = {};
      $scope.map.markers["my"] = {};
    }
    
  }

});


  disconnectRef.onDisconnect().remove(function(){
  geoFire.remove($localStorage.uData.id);
  var myid = $localStorage.uData.id;
  var mid = myid.replace(/-/gi, "");

  if(typeof $scope.map.markers !== 'undefined'){
    if(typeof $scope.map.markers[mid] !== 'undefined'){
      console.log($scope.map.markers[mid]);
      $scope.map.markers[mid] = {};
      $scope.map.markers["my"] = {};
    }
    
  }

});


function setCurrentPosition2(pos) {
              $scope.my_lat = pos.coords.latitude;
              $scope.my_lng = pos.coords.longitude;

              $rootScope.my_lat = pos.coords.latitude;
              $rootScope.my_lng = pos.coords.longitude;

              $scope.map.markers["my"] = {
                lat: $scope.my_lat,
                lng: $scope.my_lng,
                message: 'Это Я!',
                focus: false
              };

            }

            function displayAndWatch2(position) {
              console.log(position);
                setCurrentPosition2(position);
                watchCurrentPosition2();
            }

            function watchCurrentPosition2() {

                $rootScope.positionTimer2 = navigator.geolocation.watchPosition(
                  function (position) {
                      setMarkerPosition2 (position);
                  }, locError, posOptions);


            }

            function setMarkerPosition2(position) {

              

            if($rootScope.isClient){
                $scope.map.markers["my"] = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
                  message: 'Это Я!',
                  focus: false
              };
            }else{
                $scope.map.markers["my"] = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
                  message: 'Это Я!',
                  focus: false
                };
            }

            console.log(position.coords.longitude);
                
            }



           
//          $timeout(function() {
// navigator.geolocation.getCurrentPosition(displayAndWatch2, function(err){
//   console.log(err);
// });

// });




  $scope.satMap = function(){
    $scope.map.tiles = tilesDict.satellite;
  };

  $scope.baseMap = function(){
     $scope.map.tiles = tilesDict.openstreetmap;
  };

  var template = "<ion-popover-view class='popover-my'><ion-content style='padding:15px'> </ion-content></ion-popover-view>";

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });

  // .fromTemplateUrl() method
  $ionicPopover.fromTemplateUrl(template, {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.isPop = false;

  $scope.openPopover = function($event) {
    $scope.popover.show($event);

    $scope.isPop = true;

  };
  $scope.closePopover = function() {
    $scope.popover.hide();

  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();


  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
    $scope.isPop = false;
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });
    

    if($rootScope.wanted){
        $rootScope.wanted = true;
        $scope.title = 'Отменить';
    }else{
        $rootScope.wanted = false;
        $scope.title = 'Хочу такси';
    }


    $scope.wannaTaxi = function(){

      $rootScope.wanted = !$rootScope.wanted;

      if($rootScope.wanted){
            Utils.show();

       
          navigator.geolocation.getCurrentPosition(function(res){
            

            $scope.title = 'Отменить';
            // tax.child('clients').update({
            //   id: $localStorage.uData.id,
            //   lat: res.coords.latitude,
            //   lng: res.coords.longitude
            // });

            

            Utils.hide();


            $rootScope.isClient = true;

            $scope.map.markers["my"] = {
                  lat: res.coords.latitude,
                  lng: res.coords.longitude,
                  message: 'Это Я!',
                  focus: false
              };


            Utils.alertshow("Внимание!", "теперь ваше местоположение на карте видно водителям такси");

            clients.child($localStorage.uData.id).update({
              id: $localStorage.uData.id,
              name: $localStorage.uData.name,
              phone: phone[0],
              l: {
                0: res.coords.latitude,
                1: res.coords.longitude
              }
            });

          

          

          }, locError);
       
      } else{


        console.log('NOT CLIENT');
        $scope.title = 'Хочу такси!';

        $scope.map.markers[mid] = {};
       
        $scope.map.markers["my"] = {

        };

        $rootScope.isClient = false;
        clients.child($localStorage.uData.id).remove();
      }
      
    };

$scope.shareText = 'Старт';
$scope.shareState = true;


$scope.$on('$stateChangeStart', 
function(event, toState, toParams, fromState, fromParams){ 

  navigator.geolocation.clearWatch($rootScope.positionTimer);
  geoFire.remove($localStorage.uData.id);
})



    $scope.toggleShare = function(){
      $scope.shareState = !$scope.shareState;
      console.log($scope.shareState);
      if($scope.shareState == true){
        $scope.shareText = 'Старт';
        
        console.log(mid);
        navigator.geolocation.clearWatch($rootScope.positionTimer);
        geoFire.remove($localStorage.uData.id);


        leafletData.getMap('map').then(function(map){
          if($scope.route){
            map.removeControl($scope.route);
          }
        });


        console.log($scope.map.markers);
        $scope.map.markers[mid] = null;
        $scope.map.markers['my'] = null;
        $scope.map.markers['undefined'] = null;
      }else{
        $scope.shareText = 'Стоп';
        Firebase.goOnline();

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(displayAndWatch, locError);
        } else {
            Utils.alertshow("Error", "Your browser does not support the Geolocation API");
            alert("Your browser does not support the Geolocation API");
        }
      }
    };



    

    // if($localStorage.uData.id != null){
    //   var dref = 'https://blinding-heat-2337.firebaseio.com/taxi/' + $localStorage.uData.id;
    //   var disconnectRef = new Firebase(dref);
    //   console.log('https://blinding-heat-2337.firebaseio.com/taxi/' + $localStorage.uData.id);
    //     disconnectRef.onDisconnect().remove(function(){
    //       geoFire.remove($localStorage.uData.id);
    //       var myid = $localStorage.uData.id;
    //       var mid = myid.replace(/-/gi, "");

    //       $scope.map.markers[mid] = null;

    //     });
    // }
    

    // clients.on("value", function(snap){

    //             console.log(snap.val());

    //   var html = "<span style='font-weight:bold'>"+ snap.val().name +"</span><br><span>Телефон: "+ snap.val().phone +"</span><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi("+ snap.val().l[0] +","+ snap.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi()'>Разъеденить</button>";


    //       console.log(snap.key());
    //       var myid = snap.key();
    //       var clid = myid.replace(/-/gi, "");
    //       $scope.map.markers[clid] = {
    //         lat: snap.val().l[0],
    //         lng: snap.val().l[1],
    //         message: html,
    //         getMessageScope: function () {
    //             return $scope;
    //         },
    //         compileMessage: true,
    //         focus: 'false'
    //       };
    //       console.log('read CLIENT');
    // });

    // if taxi mode, listen for changes in clients
    // if($localStorage.uData.mode == 'Водитель такси'){
        

      if($localStorage.uData.mode == 'Водитель такси'){


        
        fRef.on("child_added", function(childSnap){

          $scope.$apply(function () {


          if(childSnap.val().id == mid){
            if(childSnap.val().l){
              var lat = childSnap.val().l[0];
              var lng = childSnap.val().l[1];
              console.log('addedtaxi');
              $scope.map.markers[childSnap.val().id] = {
                lat: lat,
                lng: lng,
                message: "<span style='font-weight:bold'>"+ childSnap.val().number +"</span><br><span>Модель: "+ childSnap.val().carmodel +"</span><br><span>Номер: "+ childSnap.val().carnumber +"</span><br><a class='button button-small button-balanced button-call' href='tel:"+ childSnap.val().number +"'>Позвонить</a><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi("+ childSnap.val().id +","+ childSnap.val().l[0] +", "+ childSnap.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi()'>Разъеденить</button>",
                getMessageScope: function () {
                    return $scope;
                },
                compileMessage: true,
                focus: false,
                icon: {
                  iconUrl: 'img/taxi.png',
                  iconSize: [32, 32], 
                }
              }
            }
          }

        });

        });


        fRef.on("child_changed", function(res){

          $scope.$apply(function () {

          if(res.val().id == mid){
              var lat = res.val().l[0];
              var lng = res.val().l[1];
              if(res.val().l){
              if(typeof res.val().isBusy !== 'undefined'){
                console.log('changed taxi');
                $scope.map.markers[res.val().id] = {
                  lat: lat,
                  lng: lng,
                  getMessageScope: function () {
                      return $scope;
                  },
                  message: "<span style='font-weight:bold'>"+ res.val().number +"</span><br><span>Модель: "+ res.val().carmodel +"</span><br><span>Номер: "+ res.val().carnumber +"</span><br><a class='button button-small button-balanced button-call' href='tel:"+ res.val().number +"'>Позвонить</a><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi("+ res.val().l[0] +","+ res.val().l[1] +", "+ res.val().id +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi()'>Разъеденить</button>",
                  compileMessage: true,
                  focus: false,
                  icon: {
                    iconUrl: 'img/taxi-busy.png',
                    iconSize: [32, 32], 
                  }
                }
              }else{
                console.log('changed taxi');
                $scope.map.markers[res.val().id] = {
                  lat: lat,
                  lng: lng,
                  getMessageScope: function () {
                      return $scope;
                  },
                  message: "<span style='font-weight:bold'>"+ res.val().number +"</span><br><span>Модель: "+ res.val().carmodel +"</span><br><span>Номер: "+ res.val().carnumber +"</span><br><a class='button button-small button-balanced button-call' href='tel:"+ res.val().number +"'>Позвонить</a><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi("+ res.val().l[0] +","+ res.val().l[1] +", "+ res.val().id +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi()'>Разъеденить</button>",
                  compileMessage: true,
                  focus: false,
                  icon: {
                    iconUrl: 'img/taxi.png',
                    iconSize: [32, 32], 
                  }
                }
              }

            }
          }

        });

        });


      

        clients.on('child_added', function(snap){

          if(typeof snap.val().invisible == 'undefined'){
              if(typeof snap.val().l !== 'undefined'){
                var html = "<span style='font-weight:bold'>"+ snap.val().name +"</span><br><span>Телефон: <br>"+ snap.val().phone +"</span><br><button class='button button-small button-balanced button-call'  href='tel:"+ snap.val().phone +"'>Позвонить</button><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi("+ snap.val().l[0] +","+ snap.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi()'>Разъеденить</button>";
                console.log(snap.val());
                var myid = snap.key();
                var clid = myid.replace(/-/gi, "");
                console.log(clid);
                $scope.map.markers[snap.val().g] = {
                  lat: snap.val().l[0],
                  lng: snap.val().l[1],
                  message: html,
                  getMessageScope: function () {
                      return $scope;
                  },
                  compileMessage: true,
                  focus: 'false'
                };
                console.log('added CLIENT');
            }
          }

        });


        


  clients.on('child_changed', function(snap){

          if(typeof snap.val().l !== 'undefined'){

          if(typeof snap.val().invisible == 'undefined'){

          var html = "<span style='font-weight:bold'>"+ snap.val().name +"</span><br><span>Телефон: <br>"+ snap.val().phone +"</span><br><button class='button button-small button-balanced button-call'  href='tel:"+ snap.val().phone +"'>Позвонить</button><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi("+ snap.val().l[0] +","+ snap.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi()'>Разъеденить</button>";

          var myid = snap.key();
          var clid = myid.replace(/-/gi, "");
          $scope.map.markers[snap.val().g] = {
            lat: snap.val().l[0],
            lng: snap.val().l[1],
            message: html,
            getMessageScope: function () {
                return $scope;
            },
            compileMessage: true,
            focus: 'false'
          };

}


}

    });



        

        clients.on('child_removed', function(snap){
          var myid = snap.key();
          var clid = myid.replace(/-/gi, "");

          if(typeof snap.val().g !== 'undefined'){

            $scope.map.markers[snap.val().g] = {};

          }


        });

}


    $scope.callTaxi = function(){

    };


    fRef.child($localStorage.uData.id).child('clients').on("child_added", function(childSnap, prevChild){
          if(childSnap.val().l){
            console.log(childSnap.val());
          var html = "<span style='font-weight:bold'>"+ childSnap.val().name +"</span><br><span>Телефон: <br>"+ childSnap.val().number +"</span><br><button class='button button-small button-balanced button-call'  href='tel:"+ childSnap.val().number +"'>Позвонить</button>";
            $scope.map.markers[childSnap.val().id] = {
              lat: childSnap.val().l[0],
              lng: childSnap.val().l[1],
              getMessageScope: function () {
                  return $scope;
              },
              message: html,
              compileMessage: true,
              focus: false,
              icon: {
                iconUrl: 'img/marker.png',
                iconSize: [25, 41], 
              }
            }
          }
        });

        fRef.child($localStorage.uData.id).child('clients').on("child_changed", function(childSnap, prevChild){
          if(childSnap.val().l){
          var html = "<span style='font-weight:bold'>"+ childSnap.val().name +"</span><br><span>Телефон: <br>"+ childSnap.val().number +"</span><br><button class='button button-small button-balanced button-call'  href='tel:"+ childSnap.val().number +"'>Позвонить</button>";
            $scope.map.markers[childSnap.val().id] = {
              lat: childSnap.val().l[0],
              lng: childSnap.val().l[1],
              getMessageScope: function () {
                  return $scope;
              },
              message: html,
              compileMessage: true,
              focus: false,
              icon: {
                iconUrl: 'img/marker.png',
                iconSize: [25, 41], 
              }
            }
          }
        });

        fRef.child($localStorage.uData.id).child('clients').on("child_removed", function(childSnap, prevChild){
          $scope.map.markers[childSnap.val().id] = {};
          $scope.map.markers["undefined"] = {};
          console.log('removed client');
          console.log(childSnap.val().id);
          console.log($scope.map.markers);
        });



    if($localStorage.uData.mode == 'Пешеход'){

        fRef.on('child_changed', function(childSnapshot, prevChildKey) {
         //$timeout(function again() { 
        $scope.$apply(function () {

        if($scope.map.markers["undefined"]){
          $scope.map.markers["undefined"] = {};
        }

          if(typeof childSnapshot.val().l !== 'undefined'){
            console.log('child changed');
            var phonenum = $localStorage.uData.email.match(/\d+/g);
            var lat = childSnapshot.val().l[0];
            var lng = childSnapshot.val().l[1];
              

            if(typeof childSnapshot.val().isBusy !== 'undefined'){
              $scope.map.markers[childSnapshot.val().id] = {
                  lat: lat,
                  lng: lng,
                  message: "<span style='font-weight:bold'>"+ childSnapshot.val().number +"</span><br><span>Модель: "+ childSnapshot.val().carmodel +"</span><br><span>Номер: "+ childSnapshot.val().carnumber +"</span><br><a class='button button-small button-balanced button-call' href='tel:"+ childSnapshot.val().number +"'>Позвонить</a><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi(\"" + childSnapshot.key() + "\", "+ childSnapshot.val().l[0] +","+ childSnapshot.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi(\"" + childSnapshot.key() + "\")'>Разъеденить</button>",
                  getMessageScope: function () {
                      return $scope;
                  },
                  compileMessage: true,
                  focus: false,
                  icon: {
                    iconUrl: 'img/taxi-busy.png',
                    iconSize: [32, 32], 
                  }
              }
            }else{
              $scope.map.markers[childSnapshot.val().id] = {
                  lat: lat,
                  lng: lng,
                  message: "<span style='font-weight:bold'>"+ childSnapshot.val().number +"</span><br><span>Модель: "+ childSnapshot.val().carmodel +"</span><br><span>Номер: "+ childSnapshot.val().carnumber +"</span><br><a class='button button-small button-balanced button-call' href='tel:"+ childSnapshot.val().number +"'>Позвонить</a><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi(\"" + childSnapshot.key() + "\", "+ childSnapshot.val().l[0] +","+ childSnapshot.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi(\"" + childSnapshot.key() + "\")'>Разъеденить</button>",
                  getMessageScope: function () {
                      return $scope;
                  },
                  compileMessage: true,
                  focus: false,
                  icon: {
                    iconUrl: 'img/taxi.png',
                    iconSize: [32, 32], 
                  }
              }
            }

              




          }



    });
          
        });

        





        fRef.on('child_added', function(childSnap, prevChild){


          if(typeof childSnap.val().l != 'undefined'){
            var lat = childSnap.val().l[0];
            var lng = childSnap.val().l[1];
          }
          

          console.log(childSnap.val());

    // "+ childSnap.val().l[0] +", "+ childSnap.val().l[1] +"
          if(childSnap.val().l){
            console.log('addedtaxi');


            $scope.map.markers[childSnap.val().id] = {
              lat: lat,
              lng: lng,
              message: "<span style='font-weight:bold'>"+ childSnap.val().number +"</span><br><span>Модель: "+ childSnap.val().carmodel +"</span><br><span>Номер: "+ childSnap.val().carnumber +"</span><br><a class='button button-small button-balanced button-call' href='tel:"+ childSnap.val().number +"'>Позвонить</a><br><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectTaxi("+ childSnap.val().id +","+ childSnap.val().l[0] +", "+ childSnap.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectTaxi()'>Разъеденить</button>",
              getMessageScope: function () {
                  return $scope;
              },
              compileMessage: true,
              focus: false,
              icon: {
                iconUrl: 'img/taxi.png',
                iconSize: [32, 32], 
              }
            }
          }



    });


    }

    


            function locError(error) {
                // the current position could not be located
                //alert("The current position could not be found!");
                //Utils.alertshow("Ошибка", "Включите геоданные и перезайдите в приложение");
            }


            console.log($localStorage.uData);

$scope.disconnectTaxi = function(idd){
  $scope.connected = false;
  console.log('disconnected');

  //delete isbusy property from the taxi child

  var busyRef = new Firebase("https://blinding-heat-2337.firebaseio.com/taxi/" + idd + "/clients");
  var busy = new Firebase("https://blinding-heat-2337.firebaseio.com/taxi/" + idd);

  console.log(idd);

  busy.update({
    isBusy: null
  });
  busyRef.child($localStorage.uData.id).remove();

  leafletData.getMap('map').then(function(map){
    map.removeControl($scope.route);
  });

};




$scope.connectTaxi = function(idd, lat, lon){


      var clienta = new Firebase("https://blinding-heat-2337.firebaseio.com/taxi/"+ idd + "/clients");
      var cliFire = new GeoFire(clienta);


      $scope.connected = true;
      console.log($rootScope.my_lat);
      console.log($rootScope.my_lng);

      console.log(lat + lon);
      console.log(idd);

      clients.child($localStorage.uData.id).update({
        invisible: true
      });


      //set isbusy property to the taxi child
      fRef.child(idd).update({
        isBusy: true
      });

      navigator.geolocation.getCurrentPosition(function(res){
        
        var myid = $localStorage.uData.id;
        var clid = myid.replace(/-/gi, "");



          fRef.child(idd).child('clients').child($localStorage.uData.id).update({
            'name': $localStorage.uData.name,
            'number': phone[0],
            'id': clid,
            'l': {
              '0': res.coords.latitude,
              '1': res.coords.longitude
            }
          });


        leafletData.getMap('map').then(function(map) {
          console.log('Map loaded, getting exitingly extended!');
            $scope.route = new L.Routing.control({
              createMarker: function() { return null; },
              waypoints: [
                  L.latLng(res.coords.latitude, res.coords.longitude),
                  L.latLng(lat, lon)
              ],
              autoRoute: true,

          });
          map.addControl($scope.route);

          L.Routing.Plan({
            addWaypoints: false,
            createMarker: function(){
              return null;
            },

          }).addTo(map);


          console.log($scope.map.controls);

        });
      });

      var ff = new Firebase("https://blinding-heat-2337.firebaseio.com/profile");
      

};

          





            function setCurrentPosition(pos) {
              $scope.my_lat = pos.coords.latitude;
              $scope.my_lng = pos.coords.longitude;

              console.log('setted');


                fRef.child($localStorage.uData.id).once("value", function(data){
                  $scope.isbusy = data.child("isBusy").exists();
                  console.log($scope.isbusy);
                });

                var myid = $localStorage.uData.id;
                var mid = myid.replace(/-/gi, "");
                console.log(mid);
                fRef.child($localStorage.uData.id).update({
                  'id': mid,
                  'carnumber': $localStorage.uData.carnumber,
                  'carmodel': $localStorage.uData.carmodel,
                  'number': phone[0],
                  'l': {
                    '0': pos.coords.latitude,
                    '1': pos.coords.longitude
                  }
                }); 

            }

            function displayAndWatch(position) {
              console.log(position);
                setCurrentPosition(position);
                watchCurrentPosition();
            }

            function watchCurrentPosition() {

                $rootScope.positionTimer = navigator.geolocation.watchPosition(
                  function (position) {
                      setMarkerPosition (position);
                  }, locError, posOptions);


            }

            function setMarkerPosition(position) {

                var myid = $localStorage.uData.id;
                var mid = myid.replace(/-/gi, "");
                console.log($localStorage.uData);
      
                fRef.child($localStorage.uData.id).once("value", function(data){
                  $scope.isbusy = data.child("isBusy").exists();
                  console.log($scope.isbusy);
                });


                if($scope.isbusy){

                      fRef.child($localStorage.uData.id).update({
                        'id': mid,
                        'carnumber': $localStorage.uData.carnumber,
                        'carmodel': $localStorage.uData.carmodel,
                        'number': phone[0],
                        'isBusy': true,
                        'l': {
                          '0': position.coords.latitude,
                          '1': position.coords.longitude
                        }
                      });
                }else{

                      console.log(position.coords.latitude);
                      console.log(position.coords.longitude);
                      fRef.child($localStorage.uData.id).update({
                        'id': mid,
                        'carnumber': $localStorage.uData.carnumber,
                        'carmodel': $localStorage.uData.carmodel,
                        'number': phone[0],
                        'l': {
                          '0': position.coords.latitude,
                          '1': position.coords.longitude
                        }
                      });
                  
                }
                
            }

})

.controller('CustomCtrl', function($scope, $ionicPopup, $localStorage, $ionicPopover, Utils){

})
.controller('AccountCtrl', function($scope, $window, $cordovaLocalNotification, $state, $rootScope, $q, $localStorage, leafletData, $ionicPopover, $ionicPopup, $timeout, $ionicFilterBar, $cordovaGeolocation, Utils) {
 
  var fRef = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/" + $localStorage.userkey + "/connections");
  var users = new Firebase("https://blinding-heat-2337.firebaseio.com/profile");
  var geoFire = new GeoFire(users);
  var fr = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/" + $localStorage.userkey); 
  var myid = $localStorage.uData.id;
  var posOptions = {enableHighAccuracy: true, frequency: 1700};
  $scope.isSearch = false;
  var filterBarInstance;
  var itemsProcessed = 0;
  var userid = 0;
  $scope.myUsers = new Array();
  $scope.mynum = $localStorage.uData.email.match(/^[0-9]+/);
  var id;
  var mykeys = new Array();
  var tilesDict = {
        openstreetmap: {
            url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            options: {
                attribution: ''
            }
        },
        opencyclemap: {
            url: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",
            options: {
                attribution: 'All maps &copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, map data &copy; <a href="http://www.openstreetmap.org">OpenStreetMap</a> (<a href="http://www.openstreetmap.org/copyright">ODbL</a>'
            }
        },
        satellite: {
          url: "http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}",
          options: {
            attribution: '',
            subdomains: ['mt0','mt1','mt2','mt3'],
            maxZoom: 20
          }
        }
};


// $scope.startGeolocation = function(){
//     bgGeoService.initialize();
//     bgGeoService.start();
//   };



// $scope.startGeolocation();




if($localStorage.userkey != null){
        var dref = 'https://blinding-heat-2337.firebaseio.com/profile/' + $localStorage.userkey;
        var disconnectRef = new Firebase(dref);
        console.log(dref);
}
disconnectRef.onDisconnect().update({}, function(){


//  angular.forEach($rootScope.ikeys, function(value, key){
//       if(typeof value.key !== 'undefined'){

//         users.child(value.key).child('connections').child($localStorage.uData.id).update({
//           online: false
//         });


//       }
// });


});





$scope.add = function() {
      var alarmTime = new Date();
      alarmTime.setMinutes(alarmTime.getMinutes());
      $cordovaLocalNotification.add({
          id: "1234",
          date: alarmTime,
          message: "Новое входящее соединение",
          title: "Мой гид",
          autoCancel: true,
          sound: "file://alarm.mp3"
      }).then(function () {
          console.log("The notification has been set");
      });
  };




  $scope.add2 = function() {
      var alarmTime = new Date();
      alarmTime.setMinutes(alarmTime.getMinutes());
      $cordovaLocalNotification.add({
          id: "12345",
          date: alarmTime,
          message: "Ваша заявка на местоположение была одобрена",
          title: "Мой гид",
          autoCancel: true,
          sound: "file://alarm.mp3"
      }).then(function () {
          console.log("The notification has been set");
      });
  };
 
$scope.isScheduled = function() {
    $cordovaLocalNotification.isScheduled("1234").then(function(isScheduled) {
        alert("Notification 1234 Scheduled: " + isScheduled);
    });
}



$scope.$on("$cordovaLocalNotification:added", function(id, state, json) {
    console.log("Added a notification");
});


$rootScope.$on("$cordovaLocalNotification:canceled", function(e,notification) {});
$rootScope.$on("$cordovaLocalNotification:clicked", function(e,notification) {});
$rootScope.$on("$cordovaLocalNotification:triggered", function(e,notification) {});
$rootScope.$on("$cordovaLocalNotification:added", function(e,notification) {});


  $scope.isMan = $localStorage.isMan;
  $scope.map = {
   san_fran: {
          lat: 53.22531, 
          lng: 63.5809625,
          zoom: 12
      },
      markers: {},
      tiles: tilesDict.openstreetmap,
      defaults: {
        scrollWhenZoom: false
      },
     controls: {
       routingMachine: {
            waypoints: [],
            createMarkers: function() { return null; },
            addWaypoints: false
        },
      createMarkers: function() { return null }
           
     }
  };
  $scope.satMap = function(){
    $scope.map.tiles = tilesDict.satellite;
  };
  $scope.baseMap = function(){
     $scope.map.tiles = tilesDict.openstreetmap;
  };
  var template = "<ion-popover-view id='popover' class='popover-my' style='height: 100% !important; position:relative; text-align:left; top:60% !important; margin-top:-18em; width: 80%;'><ion-content style='padding:15px; line-height:2em'>Ваше местоположение без одобрения никому не видно. <br> <div class='fa fa-map-marker leaflet-bar' style='padding:5px 10px 5px 10px'></div> – мое местоположение.<br> <div style='margin-top:5px; position:relative;' class='button icon ion-android-search button-outline button-royal button-small sear '></div>  – найти друзей, <br> возможно при условии если у ваших друзей установлено данное приложение, при необходимости посоветовать установить.<br> <div style='' class='button icon ion-android-hand button-outline button-royal button-small sear'></div> – активные соединения, <br> или запросы на соединения. В случае запроса вашего местоположения вам придет уведомление и при переходе на данную страницу вы можете одобрить или отклонить запрос. В случае имеющейся информации меняет цвет на красный. <br> Внимание – уведомления приходят если приложение свернуто, и находится во вкладках, либо вы им пользуетесь на данный момент. В тех же случаях приложение является активным. <br> При выезде за пределы населенного пункта будьте внимательны с выбором маршрута, рекомендуется пользоваться спутниковой картой. Если на карте вы не обнаружили тех или иных объектов можете воспользоваться клавишей спутник в верхнем правом углу страницы.</ion-content></ion-popover-view>";
  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });

  $ionicPopover.fromTemplateUrl(template, {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;

  var popover = document.getElementsByClassName('popover-my')[0];
  popover.setAttribute('style', 'top:3em !important;');

  });
  $scope.isPop = false;
  $scope.openPopover = function($event) {
    $scope.popover.show($event);
    $scope.isPop = true;
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
    $scope.isPop = false;
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });

console.log($localStorage.userkey);

var newItems = false;
var display = true;

var onValChange = function(childSnapshot){
  if(!newItems){
    return;
  }else{
    if(childSnapshot.val().connected == true){
        display = false;
        newItems = false;     
        console.log(childSnapshot.val());
        var connects = document.getElementById('conns');
        console.log(connects);
        connects.setAttribute('class', 'button icon ion-android-hand button-outline button-assertive button-small sear');



        return;
    }
  }
};

if(display){
  fRef.on('child_added', onValChange);
}

fRef.once("value", function(val){
  newItems = true;
  display = true;
  console.log(val.val());
})


fRef.on('child_removed', function(childSnapshot){

          fRef.once("value", function(ref){
            if(typeof ref.val() == 'undefined' || ref.val() == null || !ref.val()){
              var doc = document.getElementById('conns');
              doc.setAttribute('class', 'button icon ion-android-hand button-outline button-royal button-small sear');
            }
          });


        



  leafletData.getMap('map').then(function(map) {
    if(typeof $scope.routingMachine !== 'undefined'){
      map.removeControl($scope.routingMachine);
    } 
  });

  console.log('removed');


  if(typeof childSnapshot.val().id !== 'undefined'){
    var id = childSnapshot.val().id.replace(/-/gi, "");
    $scope.map.markers[id] = {};
  }

});


       

fRef.on('child_changed', function(childSnapshot, prevChildKey) {
      var id = childSnapshot.val().id.replace(/-/gi, "");

        console.log('changed');
        console.log($rootScope.addedPer);

      // if($rootScope.addedPer == childSnapshot.val().id){
        
          leafletData.getMap('map').then(function(map) {
          console.log('loaded map and then...');

            if(typeof $scope.routingMachine !== 'undefined'){
              map.removeControl($scope.routingMachine);
            }

            $scope.routingMachine = new L.Routing.control({
              waypoints: [
                  L.latLng(childSnapshot.val().l[0], childSnapshot.val().l[1]),
                  L.latLng($rootScope.lat, $rootScope.lon)
              ],
              createMarker: function() { return null; },
              addWaypoints: false,
              fitSelectedRoutes: false,
              autoRoute: true
          });
          console.log($scope.map.controls);
          map.addControl($scope.routingMachine);
        });


      
      var doc = document.getElementById('conns');
      doc.setAttribute('class', 'button icon ion-android-hand button-outline button-assertive button-small sear');


      var html = "<span style='font-weight:bold'>"+ childSnapshot.val().name +"</span><br><span>"+ childSnapshot.val().phone +"</span><br><a class='button button-small button-balanced button-call' href='tel:"+ childSnapshot.val().phone +"'>Позвонить</a>";
      $scope.map.markers[id] = {
        lat: childSnapshot.val().l[0],
        lng: childSnapshot.val().l[1],
        message: html,
        getMessageScope: function () {
            return $scope;
        },
        compileMessage: true,
      };
        


        

});

// fRef.once("value", function(snapshot){
//   snapshot.forEach(function(snap){
//     console.log('have connection val');
//     if(typeof snap.val().l !== 'undefined'){
//       var id = snap.val().id.replace(/-/gi, "");
//       var html = "<span style='font-weight:bold'>"+ snap.val().name +"</span><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectMan("+ snap.val().l[0] +","+ snap.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectMan()'>Разъеденить</button>";

//       $scope.map.markers[id] = {
//         lat: snap.val().l[0],
//         lng: snap.val().l[1],
//         message: html,
//         getMessageScope: function () {
//             return $scope;
//         },
//         compileMessage: true,
//       }
//     }
//   });
// });

$scope.changeState = 'На карту';

$scope.clicked = function(){
  console.log('clicked');
  $scope.changeState = 'На карту';
};

$scope.removePerson = function(key, id){
  $scope.changeState = 'Применить';
  var user = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey);
  user.child('connections').once("value", function(data){
    data.forEach(function(dataSnap){
      if(dataSnap.val().key == key){
        user.child('connections').child(dataSnap.key()).remove();
      }
    });
  });
  var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key + "/keys/"+ $localStorage.userkey);
  keys.remove();
  console.log(id);
  $scope.conns[key] = null;
};

$scope.delete = function (index) {
  $scope.conns.splice(index, 1);
}



$scope.addPerson = function(name, id, lat, lng, key){
  $scope.added = true;
  $scope.changeState = 'Применить';

  var user = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey);
  user.child('keys').push({
    connected: true,
    id: id,
    key: key,
    name: name
  });
  
  navigator.geolocation.getCurrentPosition(function(res){
    leafletData.getMap('map').then(function(map) {
      console.log('Map loaded, getting exitingly extended!');
        $scope.connected = true;
        $scope.routingMachine = new L.Routing.control({
          waypoints: [
              L.latLng(res.coords.latitude, res.coords.longitude),
              L.latLng(lat, lng)
          ],
          createMarker: function() { return null; },
          addWaypoints: false
      });
      console.log($scope.map.controls);
      map.addControl($scope.routingMachine);
    });
  });
};

//collect users to add
users.once("value", function(snapshot) {
  snapshot.forEach(function(snap) {
      if(snap.val().email && snap.val().mode == 'Пешеход'){
        var myid = snap.val().email.match(/^[0-9]+/gi)[0];
        $scope.myUsers[userid++] = {
          phone: myid,
          name: snap.val().name,
          key: snap.key()
        }
      } 
  });
});


//loop through all connections and render it

// if(typeof $localStorage.userkey !== 'undefined'){
//   users.child($localStorage.userkey).child('connections').once("value", function(dataSnap){
//   dataSnap.forEach(function(childSnap){
//       if(childSnap.val().connected == false){
//          var html = "<span style='font-weight:bold'>"+ childSnap.val().name +"</span><button class='button button-small button-positive button-call' ng-if='!connected' ng-click='connectMan("+ childSnap.val().l[0] +","+ childSnap.val().l[1] +")'>Соеденить</button><button class='button button-small button-positive button-call' ng-if='connected' ng-click='disconnectMan()'>Разъеденить</button>";
//          var id = childSnap.val().id.replace(/-/gi, "");
//          console.log(html);
//           $scope.map.markers[id] = {
//             lat: childSnap.val().l[0],
//             lng: childSnap.val().l[1],
//             message: html,
//             getMessageScope: function () {
//                 return $scope;
//             },
//             compileMessage: true,
//             focus: false
//           }
//       }
//     });
//   });
// }


$scope.disconnectMan = function(){
  $scope.connected = false;
  console.log('disconnected');

  leafletData.getMap('map').then(function(map){
    map.removeControl($scope.routingMachine);
  });

};

$scope.connectMan = function(lat, lon){
  $scope.connected = true;
  navigator.geolocation.getCurrentPosition(function(res){
    leafletData.getMap('map').then(function(map) {
      console.log('Map loaded, getting exitingly extended!');
        $scope.routingMachine = new L.Routing.control({
          waypoints: [
              L.latLng(res.coords.latitude, res.coords.longitude),
              L.latLng(lat, lon),
          ],
          createMarker: function() { return null; }
      });
      console.log($scope.map.controls);
      map.addControl($scope.routingMachine);


    });
  });
};

  



$scope.sendReq = function(phone, name, key){
  var email = phone + '@mail.com';
//edit gf setting (other fields in bd)
    
      
        var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey +"/keys");
        var connect = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key +"/connections");
        var geoFire = new GeoFire(connect);
        var gf = new GeoFire(keys);
        //gf.set(key, [0, 1]);
        var sendkey = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key +"/keys");

        console.log(name);
        console.log(phone);
        console.log(key);

        if(name){
          keys.push({
            connected: false,
            key: key,
            name: name,
            phone: phone,
            outgoing: true
          });
        }else{
          keys.push({
            connected: false,
            key: key,
            phone: phone,
            outgoing: true
          });
        }


        if($localStorage.uData.name){
          users.child(key).child('connections').child($localStorage.uData.id).update({
            connected: true,
            id: $localStorage.uData.id,
            name: $localStorage.uData.name,
            key: $localStorage.userkey,
            phone: $scope.mynum[0]
          });
        }else{
          users.child(key).child('connections').child($localStorage.uData.id).update({
            connected: true,
            id: $localStorage.uData.id,
            key: $localStorage.userkey,
            phone: $scope.mynum[0]
          });
        }

       $state.reload();

        var list = document.getElementsByClassName('index');
        list[0].setAttribute('style', 'display:none');

  };


  $scope.isSearch = false;

  $scope.showFilterBar = function () {
    $rootScope.res = null;

    var list = document.getElementsByClassName('list');
        list[0].setAttribute('style', 'display:visible');

    $scope.isSearch = true;
    filterBarInstance = $ionicFilterBar.show({
      cancelText: 'Отмена',
      items: $scope.myUsers,
      update: function(filteredItems, filterText){
          console.log($rootScope.res);
          if(filterText){
            $scope.nores = false;
            if(filterText.replace(/\s/g, "").length == '7'){
              $rootScope.res = filteredItems[0];
              console.log($scope.res);

            }
          }else{
            $scope.nores = true;
          }
          
      },
       cancel: function(){
          $scope.isSearch = false;
          console.log('cancelled');
          $rootScope.res = false;
       }
    });
  };


      leafletData.getMap('map').then(function(map) {
        var lc = L.control.locate({ follow: true,
          drawCircle: true,
          drawMarker: true,
          color: '#886aea',
          circleStyle: {
                      color: '#886aea',

            fillOpacity: 0,
            weight: 2,
            fillColor: 'red'
          },
          followCircleStyle: {
                      color: '#886aea',

            fillColor: 'red'
          },
          followMarkerStyle: {
            color: '#886aea',
            fillColor: 'purple'
          },
          locateOptions: {
            watch: true,
            maxZoom: 15
          }
         }).addTo(map);
        lc.start();
        map.locate();
        console.log('set get');
      });


  //check if navigator.bggeo is available
function onDeviceReady() {
 
  cordova.plugins.backgroundMode.enable();

}

document.addEventListener('deviceready', onDeviceReady, false);

      navigator.geolocation.getCurrentPosition(displayAndWatch, locError);




  function locError(error){};

  $scope.center = {
    lat: $scope.my_lat,
    lng: $scope.my_lng,
    zoom: 12
  };


  function setCurrentPosition(pos) {
      $scope.my_lat = pos.coords.latitude;
      $scope.my_lng = pos.coords.longitude;


      leafletData.getMap('map').then(function(map) {
        console.log('Map loaded, getting exitingly extended!');
        console.log('setted currPOS');
      });

      $scope.map.san_fran = {
        lat: pos.coords.latitude,
        lng: pos.coords.longitude,
        zoom: 12
      }

      var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey + "/keys");


//restore name option
      keys.once("value", function(snapshot) {
        snapshot.forEach(function(child) {
          if(child.val().connected == true){
            console.log(child.val().key);
            var conn = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ child.val().key + "/connections"); 
            var gf = new GeoFire(conn);

            if($localStorage.uData.name){

                users.child(child.val().key).child('connections').child($localStorage.uData.id).update({
                  connected: false,
                  id: $localStorage.uData.id,
                  name: $localStorage.uData.name,
                  key: $localStorage.userkey,
                  phone: $scope.mynum[0],
                  l: {
                    0: $scope.my_lat,
                    1: $scope.my_lng
                  }
                });

            }else{

                users.child(child.val().key).child('connections').child($localStorage.uData.id).update({
                  connected: false,
                  id: $localStorage.uData.id,
                  key: $localStorage.userkey,
                  phone: $scope.mynum[0],
                  l: {
                    0: $scope.my_lat,
                    1: $scope.my_lng
                  }
                });

              
            }

           
          }
          
        });
      });      
  }
  function displayAndWatch(position) {
      //setCurrentPosition(position);
      watchCurrentPosition();
  }

  function watchCurrentPosition() {
    $rootScope.pt = navigator.geolocation.watchPosition(function (position) {
        setMarkerPosition (position);
      }, locError, posOptions);
  }



  var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey + "/keys");

  keys.on("child_removed", function(child){
    angular.forEach($rootScope.ikeys, function(value, key){
      if(value.key == child.val().key){
        $rootScope.ikeys[key] = {};
        console.log('deleted '+ child.val().key);
      }
    });
  });

   keys.on("child_added", function(child){

    if(child.val().connected == true){
          $rootScope.ikeys[child.val().key] = child.val();
    }


      console.log($rootScope.ikeys);
  });


   keys.on("child_changed", function(child){
    if(child.val().connected == true){
        $rootScope.ikeys[child.val().key] = child.val();

        // push notification about added person
        //Ваша заявка на местоположение была принята

         var doc = document.getElementById('reqs');
        doc.setAttribute('class', 'button icon ion-android-search button-outline button-royal button-small sear')

        $scope.add2();

       



    }else if(child.val().connected == false){
      $rootScope.ikeys[child.val().key] = {};

      var doc = document.getElementById('reqs');
      doc.setAttribute('class', 'button icon ion-android-search button-outline button-royal button-small sear')

    }
   })

   if(typeof $rootScope.ikeys == 'undefined'){
      $rootScope.ikeys = {};
      console.log($rootScope.ikeys);
   }

  //if keys available , stream coords to users of this keys (it means that you've requested him)
   var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey + "/keys");
      keys.once("value", function(snapshot) {
        snapshot.forEach(function(child) {
          if(child.val().key){
            if(child.val().connected == true){

              $rootScope.ikeys[child.val().key] = child.val();
              console.log($rootScope.ikeys);

              // users.child(child.val().key).child('connections').child($localStorage.uData.id).update({
              //   connected: false,
              //   id: $localStorage.uData.id,
              //   name: $localStorage.uData.name,
              //   key: $localStorage.userkey,
              //   phone: $scope.mynum[0],
              //   l: {
              //     0: $scope.my_lat,
              //     1: $scope.my_lng
              //   }
              // });
            }
          }
        });
      });




// $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 
//   navigator.geolocation.clearWatch($rootScope.pt);
//   console.log('state changed');
//   $rootScope.ikeys = {};
// })



  function setMarkerPosition(position) {

    $scope.my_lat = position.coords.latitude;
    $scope.my_lng = position.coords.longitude;

    $rootScope.lat = position.coords.latitude;
    $rootScope.lon = position.coords.longitude;    

    console.log($rootScope.ikeys);

    angular.forEach($rootScope.ikeys, function(value, key){
      console.log(value);
      console.log(key);
      if(typeof value.key !== 'undefined'){
          users.child(value.key).child('connections').child($localStorage.uData.id).update({
          connected: false,
          id: $localStorage.uData.id,
          name: $localStorage.uData.name,
          key: $localStorage.userkey,
          phone: $scope.mynum[0],
          l: {
            0: $scope.my_lat,
            1: $scope.my_lng
          }
        });
      }
    });


  }


}).controller('ReqsCtrl', function($scope, $cordovaLocalNotification, $rootScope, $ionicPopup, $ionicPopover, $localStorage, $state, Utils){

  

  var users = new Firebase("https://blinding-heat-2337.firebaseio.com/profile");
  $scope.reqs = {};  
  Utils.show();
  var proc = 0;
  var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey + "/keys");
  var template = "<ion-popover-view class='popover-my' style='width: 80%; height: 100px !important; margin-top:-18em;'><ion-content style='padding:15px'>Введите номер телефона абонента с которым хотите установить соединение. После отправления одного или нескольких запросов можете вернуться на карту. Также можете отменить запрос, запросить повторно и в случае необходимости удалить. При отмене запрос не активен, а абонент сохраняется в исходящих соединениях. </ion-content></ion-popover-view>";
  $scope.mynum = $localStorage.uData.email.match(/^[0-9]+/);

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
  $ionicPopover.fromTemplateUrl(template, {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.isPop = false;

  $scope.openPopover = function($event) {
    $scope.popover.show($event);

    $scope.isPop = true;

    var dd = document.getElementsByClassName('popover-wrapper')[0];
  dd.setAttribute('style', 'height: auto !important');

  };
  $scope.closePopover = function() {
    $scope.popover.hide();

  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
    $scope.isPop = false;
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });


  $scope.outside = false;
  var userid = 0;
  $rootScope.isSearch = false;

  $scope.myUsers = new Array();

   users.once("value", function(snapshot) {
    snapshot.forEach(function(snap) {
        if(snap.val().email && snap.val().name){
          var myid = snap.val().email.match(/^[0-9]+/gi)[0];
          $scope.myUsers[userid++] = {
            phone: myid,
            name: snap.val().name,
            key: snap.key()
          }
        }else if(snap.val().email && snap.val().mode == 'Пешеход'){
          console.log(snap.val().email);
          if(snap.val().email !== undefined){
            var myid = snap.val().email.match(/^[0-9]+/gi)[0];
            console.log(snap.val().email.match(/^[0-9]+/gi));

            $scope.myUsers[userid++] = {
              phone: myid,
              key: snap.key()
            }
          }
        } 
    });
  });
      $scope.reqState = new Array();



  $scope.request = function(phone, key, index){

 var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey + "/keys");
 var conns = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key + "/connections");
 var hisKeys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key + "/keys");
 var myConnects = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey + "/connections");

console.log($localStorage);

    if($scope.reqState[key] == 'Отменить'){
      
      conns.once("value", function(cons){
        cons.forEach(function(con){
          console.log(con.val());
          console.log(key);
          if(con.val().key == $localStorage.userkey){
            conns.child(con.key()).remove();
            $scope.reqState[key] = 'Запросить';
          }

        });
      });


      myConnects.once("value", function(conects){
        conects.forEach(function(con){
          console.log(key);
          if(con.val().key == key){
            console.log(con.val().key);
            myConnects.child(con.key()).remove();
          }
        });
      });


      hisKeys.once("value", function(keys){
        keys.forEach(function(key){
          console.log(key);
          if(key.val().key == $localStorage.userkey){
            console.log(key.key());
            hisKeys.child(key.key()).remove();
          }
        });
      });


      $scope.reqState[key] = 'Запросить';
      
      keys.once("value", function(res){
        res.forEach(function(result){
          if(result.val().key == key){
            keys.child(result.key()).update({
              cancelled: true,
              connected: false
            });
          }
        });
      });

      console.log($scope.reqState[key]);


    }else{

          var confirm = $ionicPopup.confirm({
          title: 'Подтвердите запрос',
          template: " ",
          buttons: [
            { text: 'Отмена' ,
              onTap: function(e) {
                console.log('Cancel tapped');
              }

          },
            {
              text: '<b>Запросить</b>',
              type: 'button-royal',
              onTap: function(e) {
                var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey +"/keys");
                var connect = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key +"/connections");
                var geoFire = new GeoFire(connect);
                var gf = new GeoFire(keys);

                users.child(key).child('connections').child($localStorage.uData.id).update({
                  connected: true,
                  id: $localStorage.uData.id,
                  name: $localStorage.uData.name,
                  key: $localStorage.userkey,
                  phone: $scope.mynum[0]
                });

                $scope.reqState[key] = 'Отменить';

                keys.once("value", function(res){
                    res.forEach(function(result){
                  if(result.val().key == key){
                    keys.child(result.key()).update({
                      cancelled: false
                    });
                    }
                  });
                });
              }
            }
          ]
        });



        confirm.then(function(res){
          if(res){
            
          }

        });
    }

  };

$scope.add2 = function() {
      var alarmTime = new Date();
      alarmTime.setMinutes(alarmTime.getMinutes());
      $cordovaLocalNotification.add({
          id: "12345",
          date: alarmTime,
          message: "Ваша заявка на местоположение была одобрена",
          title: "Мой гид",
          autoCancel: true,
          sound: "file://alarm.mp3"
      }).then(function () {
          console.log("The notification has been set");
      });
  };


keys.on('child_changed', function(dataSnap){
  if(dataSnap.val().connected == true){
    $scope.add2();
  }
});



  keys.once("value", function(dataSnap){

    if(dataSnap.val() === null){
      console.log(dataSnap.key());
      Utils.hide();
    }

    dataSnap.forEach(function(childSnap){

      //if(typeof childSnap.val().name !== 'undefined'){
        if(childSnap.val().outgoing){
          console.log(childSnap.val().name);
          console.log(childSnap.val());
          proc++;
          $scope.reqs[childSnap.key()] = childSnap.val();   

          if(childSnap.val().cancelled == true){
            $scope.reqState[childSnap.val().key] = 'Запросить';
                      $scope.outside = false;

          }else{
            $scope.reqState[childSnap.val().key] = 'Отменить';

          }



          if(childSnap.val().cancelled == false){
                      $scope.outside = true;
          }

          if(childSnap.val().connected == true){
              $scope.outside = false;

          }

          console.log($scope.reqState[childSnap.val().key]);


        }    
              Utils.hide();


     if(Object.keys(dataSnap.val()).length == proc){
                    Utils.hide();

     }

    });

  });




  $scope.cancelReq = function(keynum){

var confirm = $ionicPopup.confirm({
      title: 'Подтвердите удаление',
      template: " ",
      buttons: [
      { text: 'Отмена' },
      {
        text: '<b>Удалить</b>',
        type: 'button-royal',
        onTap: function(e) {
          var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey + "/keys");
      var conns = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ keynum + "/connections");

      conns.once("value", function(cons){
        cons.forEach(function(con){
          if(con.val().key == keynum){
            conns.child(con.key()).remove();
          }
        });
      });
      keys.once("value", function(kes){
        kes.forEach(function(keysnap){
          console.log(keysnap.val());
          if(keynum == keysnap.val().key){
            keys.child(keysnap.key()).remove();
            $state.reload();
          }
        });
      });
        }
      }
    ]
    });


confirm.then(function(res){
  if(res){
    var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey + "/keys");
      var conns = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ keynum + "/connections");

      conns.once("value", function(cons){
        cons.forEach(function(con){
          if(con.val().key == keynum){
            conns.child(con.key()).remove();
          }
        });
      });
      keys.once("value", function(kes){
        kes.forEach(function(keysnap){
          console.log(keysnap.val());
          if(keynum == keysnap.val().key){
            keys.child(keysnap.key()).remove();
            $state.reload();
          }
        });
      });
  }
});

      




  };



}).controller('ConnsCtrl', function($scope, $ionicPopup, $rootScope, $ionicPopover, leafletData, $state, $localStorage, Utils){


  var users = new Firebase("https://blinding-heat-2337.firebaseio.com/profile");
    $scope.conns = {};  
    Utils.show();

    //$rootScope.ikeys = {};

    console.log($rootScope.mykes);





 $scope.removePerson = function(key, id){

  var confirm = $ionicPopup.confirm({
          title: 'Подтвердите удаление',
          template: " ",
          buttons: [
            { text: 'Отмена' },
            {
              text: '<b>Удалить</b>',
              type: 'button-royal',
              onTap: function(e) {
                  $scope.changeState = 'Применить';
    console.log(key);

    var man = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/" + key);

    var user = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey);
    user.child('connections').once("value", function(data){
      data.forEach(function(dataSnap){
        if(dataSnap.val().key == key){
          user.child('connections').child(dataSnap.key()).remove();
        }
      });
    });


    man.child('connections').once("value", function(data){
      data.forEach(function(dataSnap){
        if(dataSnap.val().key == $localStorage.userkey){
          man.child('connections').child(dataSnap.key()).remove();
        }
      });
    });

    user.child('keys').once("value", function(data){
      data.forEach(function(snap){
        if(snap.val().key == key){
          console.log('match');
          user.child('keys').child(snap.key()).update({
            cancelled: true,
            connected: false
          });
        }
      });
    });



    man.child('keys').once("value", function(mykeys){
      mykeys.forEach(function(mykey){
        console.log(mykey.key());
        if(mykey.val().key == $localStorage.userkey){
          console.log('deleted our key from spec user');
          man.child('keys').child(mykey.key()).update({
            connected: false,
            cancelled: true
          });
        }
      });
    });

    // var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key + "/keys/"+ $localStorage.userkey);
    // keys.remove();
    // console.log(key);
    // console.log(id);
    $scope.conns[key] = null;

    $state.reload();



              }
            }
          ]
        });


    
  };


  $scope.request = function(phone, key, index){

    var confirm = $ionicPopup.confirm({
      title: 'Подтвердите запрос',
      template: phone + "<br>Вы точно хотите установить соединение с данным абонентом?"
    });


    confirm.then(function(res){
      if(res){
        var keys = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey +"/keys");
        var connect = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key +"/connections");
        var geoFire = new GeoFire(connect);
        var gf = new GeoFire(keys);
        //gf.set(key, [0, 1]);

        geoFire.set($localStorage.uData.id, [$scope.my_lat, $scope.my_lng]).then(function(){
          users.child(key).child('connections').child($localStorage.uData.id).update({
            connected: false,
            id: $localStorage.uData.id,
            name: $localStorage.uData.name,
            key: key
          });
        });
      }

    });


    geoFire.set($localStorage.uData.id, [$scope.my_lat, $scope.my_lng]).then(function(){
      users.child(key).child('connections').child($localStorage.uData.id).update({
        connected: false,
        id: $localStorage.uData.id,
        name: $localStorage.uData.name,
        key: key
      });
    });


  };

//here we have to add route
  $scope.addPerson = function(phone, name, id, lat, lng, key){
    
    var confirm = $ionicPopup.confirm({
          title: 'Подтвердите добавление',
          template: " ",
          buttons: [
            { text: 'Отмена',
              onTap: function(e){}
            },
            {
              text: '<b>Добавить</b>',
              type: 'button-royal',
              onTap: function(e) {


    $scope.added = true;
    $scope.changeState = 'Применить';
    $rootScope.addedPer = id;
    var touser = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ key);
    var user = new Firebase("https://blinding-heat-2337.firebaseio.com/profile/"+ $localStorage.userkey);


      user.child('keys').once("value", function(data){
        if(data.val() !== null){

        data.forEach(function(obj){
          if(obj.val().key == key){
            user.child('keys').child(obj.key()).update({
                connected: true,
                id: id,
                name: name,
                key: key,
                phone: phone
            });
          }
        });

}else{
  user.child('keys').push({
                connected: true,
                id: id,
                name: name,
                key: key,
                phone: phone
            });
}

      });

    
    
    touser.child('keys').once("value", function(data){
      data.forEach(function(snap){
        console.log(snap.val());
        console.log($localStorage.userkey);

        if(snap.val().key == $localStorage.userkey){
          console.log(key);
          console.log(snap.val().key);
          touser.child('keys').child(snap.key()).update({
            connected: true
          });
        }
      });
    });





              }
            }
          ]
        });









  };


$scope.isConn = false;

console.log($scope.conns);
var proc = 0;

users.child($localStorage.userkey).child('connections').once("value", function(dataSnap){
  if(dataSnap.val() === null){
    console.log(dataSnap.val());
    Utils.hide();
  }else{
    console.log(dataSnap.val());

    dataSnap.forEach(function(childSnap){
          proc++;
           console.log($scope.conns);
           var id = childSnap.val().id.replace(/-/gi, "");
           $scope.conns[childSnap.key()] = childSnap.val();

           console.log(Object.keys($scope.conns).length);  
           $scope.isConn = true;  

            if(Object.keys(dataSnap.val()).length == proc){
              Utils.hide();
            }
  });

  }
  
});

var template = "<ion-popover-view class='popover-my' style='width: 80%; margin-top:-18em;'><ion-content style='padding:15px'>После соединения с абонентом при помощи клавиши «удалить» вы удаляете соединение, и данный абонент больше не видит ваше местоположение. </ion-content></ion-popover-view>";

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
  $ionicPopover.fromTemplateUrl(template, {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.isPop = false;

  $scope.openPopover = function($event) {
    $scope.popover.show($event);

    $scope.isPop = true;

    var dd = document.getElementsByClassName('popover-wrapper')[0];
  dd.setAttribute('style', 'height: auto !important');


  };
  $scope.closePopover = function() {
    $scope.popover.hide();

  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
    $scope.isPop = false;
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });






}).controller('tabs', function($scope, $localStorage){

    console.log($localStorage);

  if($localStorage.uData.mode == 'Водитель автобуса'){
    $scope.isBus = true;
    $localStorage.isBus = true;

  }else if($localStorage.uData.mode == 'Пешеход'){
    $scope.isMan = true;
    $localStorage.isMan = true;

  } else if($localStorage.uData.mode == 'Водитель такси'){
    $scope.isTaxi = true;
    $localStorage.isTaxi = true;   
  }


});
