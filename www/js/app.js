angular.module('App', ['ionic', 'App.controllers', 'jett.ionic.filter.bar', 'ngStorage', 'ngCordova','firebase','ngMessages'])
.config(function($stateProvider, $ionicConfigProvider, $ionicFilterBarConfigProvider, $urlRouterProvider) {

  $ionicConfigProvider.backButton.text('Назад');

$ionicConfigProvider.views.maxCache(0);
$ionicFilterBarConfigProvider.placeholder('Введите номер...');

$stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'views/login/login.html',
      controller:'loginController'
    })
    .state('forgot', {
      url: '/forgot',
      templateUrl: 'views/forgot/forgot.html',
      controller:'forgotController'
    })
    .state('register', {
      url: '/register',
      templateUrl: 'views/register/register.html',
      controller:'registerController'
    })
    .state('home', {
      url: '/home',
      templateUrl: 'views/home/home.html',
      controller:'homeController'
    })
    .state('profile', {
      url: "/profile/:profileId",
      templateUrl: 'views/profile.html',
      controller: 'profileController'      
    })
    .state('connections', {
      url: "/connections/:conId",
      templateUrl: 'views/connections.html',
      controller: 'ConnsCtrl'      
    })
    .state('requests', {
      url: "/requests/:reqId",
      templateUrl: 'views/requests.html',
      controller: 'ReqsCtrl'      
    })
    // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'views/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.bus', {
    url: '/bus',
    cache: false,
    views: {
      'tab-bus': {
        templateUrl: 'views/tab-bus.html',
        controller: 'BusCtrl'
      }
    }
  })

  .state('tab.taxi', {
      url: '/taxi',
      cache: false,
      views: {
        'tab-taxi': {
          templateUrl: 'views/tab-taxi.html',
          controller: 'TaxiCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    cache: false,
    views: {
      'tab-account': {
        templateUrl: 'views/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });



   if(window.localStorage['ngStorage-uData']){

     if(window.localStorage['ngStorage-uData'].match(/Пешеход/gmi)){

       if(window.localStorage['ngStorage-uData'].match(/Пешеход/gmi)[0] == 'Пешеход'){

         $urlRouterProvider.otherwise("/tab/account");
       }

     } else if(window.localStorage['ngStorage-uData'].match(/Водитель автобуса/gmi)){

       if(window.localStorage['ngStorage-uData'].match(/Водитель автобуса/gmi)[0] == 'Водитель автобуса'){

         $urlRouterProvider.otherwise("/tab/bus");
       }
     } else if(window.localStorage['ngStorage-uData'].match(/Водитель такси/gmi)){

       if(window.localStorage['ngStorage-uData'].match(/Водитель такси/gmi)[0] == 'Водитель такси'){

         $urlRouterProvider.otherwise("/tab/taxi");
       }
     }
   }else{

    $urlRouterProvider.otherwise("/login");
   }


})
// Changue this for your Firebase App URL.
.constant('FURL', 'https://blinding-heat-2337.firebaseio.com/')
.run(function($ionicPlatform) {

  
  // navigator.geolocation.getCurrentPosition(function(res){
        $ionicPlatform.ready(function() {

          
          console.log('started');
          // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
          // for form inputs)

            window.screen.lockOrientation('portrait');





          if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          }
          if(window.StatusBar) {
            StatusBar.styleDefault();
          }






        });
    // },
   // function(err){
   // // alert("Включите геоданные и перезапустите приложение!");









   // },
   // {timeout:3000});




});
