	angular.module('App').factory('Utils', function($ionicLoading,$ionicPopup) {

	var Utils = {

    show: function() {
      $ionicLoading.show({
  	    animation: 'fade-in',
  	    showBackdrop: false,
  	    maxWidth: 200,
  	    showDelay: 500,
        template: '<p class="item-icon-left">Загрузка...<ion-spinner icon="android"/></p>'
      });
    },

    hide: function(){
      $ionicLoading.hide();
    },

		alertshow: function(tit,msg){
			var alertPopup = $ionicPopup.alert({
				title: tit,
				template: msg
			});
			alertPopup.then(function(res) {
				if(res){
					alertPopup.close();
					alertPopup = null;
				}
			});

			
		},

		errMessage: function(err) {

	    var msg = "Неизвестная ошибка...";

	    if(err && err.code) {
	      switch (err.code) {
	        case "EMAIL_TAKEN":
	          msg = "Этот номер телефона уже занят"; break;
	        case "INVALID_EMAIL":
	          msg = "Неверный номер телефона"; break;
          case "NETWORK_ERROR":
	          msg = "Ошибка соединения с интернетом"; break;
	        case "INVALID_PASSWORD":
	          msg = "Неверный пароль"; break;
	        case "INVALID_USER":
	          msg = "Неверный номер или пароль"; break;
	      }
	    }
			Utils.alertshow("Ошибка: ",msg);
	},


  };

	return Utils;

});
